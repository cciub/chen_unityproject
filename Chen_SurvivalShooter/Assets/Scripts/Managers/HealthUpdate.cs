﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUpdate : MonoBehaviour {
    public Transform player;
    public PlayerHealth playerHealth;

    Text text;


    void Awake()
    {
        text = GetComponent<Text>();
        playerHealth = player.GetComponent<PlayerHealth>();
    }


    void Update()
    {
        text.text = playerHealth.currentHealth.ToString() + " / 100 ";
    }
}
