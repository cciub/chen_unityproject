﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime;
    public Transform[] spawnPoints;
    public int score;
    public bool trigger = false;

    void Start ()
    {
        InvokeRepeating ("Spawn", spawnTime, spawnTime);
    }


    void Spawn ()
    {
        if(playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range (0, spawnPoints.Length);

        Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }

 

    void Update ()
    {
        score = ScoreManager.score;

        if (score >= 100000 && score < 300000 && !trigger)
        {
            spawnTime -= 0.5f;
            trigger = true;
        }
        else if (score >= 300000 && score < 500000 && trigger)
        {
            spawnTime -= 0.5f;
            trigger = false;
        }
        else if (score >= 500000 && !trigger)
        {
            spawnTime -= 0.5f;
            trigger = true;
        }
    }
}
