﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public static float time;
    public bool live = true;
    Text text;
    

    void Awake()
    {
        text = GetComponent<Text>();
        time = 0;
    }

    void Update()
    {
        if (playerHealth.currentHealth > 0)
        {
            time += Time.deltaTime;

            var minutes = time / 60;
            var seconds = time % 60;
            var fraction = (time * 100) % 100;

            text.text = string.Format("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);

        }
    }
}
