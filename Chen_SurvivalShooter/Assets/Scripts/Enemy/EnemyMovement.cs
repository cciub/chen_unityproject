﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;
    public float timer1 = 0f;
    public float timer2 = 0f;
    public float freezeTimer = 0f;
    public int score;
    public bool trigger = false;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
        playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
    }


    void Update()
    {
        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            nav.SetDestination(player.position);
        }
        else
        {
            nav.enabled = false;
        }
     
        

        score = ScoreManager.score;
        if (score > 100000 && !trigger)
        {
            timer1 += Time.deltaTime;
            if (timer1 > 15f)
            {
                nav.speed += 3;
                timer1 = 0f;
                trigger = true;
            }
            
        }

        if (trigger)
        {
            timer2 += Time.deltaTime;
            if (timer2 > 3f)
            {
                nav.speed -= 3;
                timer2 = 0f;
                trigger = false;
            }
        }

        bool frozen = false;

        if (freezeTimer > 0f)
        {
            frozen = true;
            freezeTimer -= Time.deltaTime;

            if (freezeTimer <= 0f)
            {
                frozen = false;
                nav.enabled = true;
            }
        }

        if (!frozen && enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            nav.SetDestination(player.position);
        }
    }
}
